### Nota
Esto no es el hosting oficial de la pagina! Uso Pages de CI/CD para verificar que
la pagina se ve correctamente en todo tipo de dispositivos.

## Estructura de archivos

En la carpeta `web/` esta toda la pagina:

* `index.html`: Pagina principal, de momento de prueba.
* `css/`: Todos los archivos de estilo.
    * `style.css`: CSS principal, con el estilo de navbar, footer, main y
    titulos.
    * `containers.css`: CSS secundario con el estilo de los contenedores de
    imagen + texto.
    * `buttons.css`: CSS secundario con el estilo de los botones.
    * `carousel.css`: CSS que define el estilo de carrusel de la pagina
    principal.
* `js/`: Todos los archivos de JavaScript.
    * `navbar.js`: Codigo que muestra y oculta el menu de navegacion en movil,
    y la administra en la carga y redimension de la pagina.
    * `carousel.js`: Establece las dimensiones del carrusel y sus imagenes, y
    permite avanzarlo automaticamente y manualmente.
* `img/`: Todas las imagenes de la pagina.
    * `logos/`: logos en distintos formatos.