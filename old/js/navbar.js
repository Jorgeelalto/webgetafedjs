// Cierra o abre la barra de navegacion en movil
function handle_menu() {
    // Recuperamos los links de la barra
    var l = document.getElementsByClassName("nav-link");
    // Si el primero se esta mostrando, eso es que la barra de navegacion
    // esta abierta. Oculta todos los links ("cierra la barra")
    if (l[0].style.display === "block") {
        for (let i of l) {
            i.style.display = "none";
        }
        // Y cambiamos el icono
        document.getElementById("nav-handle-icon").classList.remove("fa-chevron-up");
        document.getElementById("nav-handle-icon").classList.add("fa-chevron-down");
    // Si el primero esta oculto, eso es que la barra esta cerrada. Muestra
    // todos los links ("abre la barra")
    } else {
        for (let i of l) {
            i.style.display = "block";
        }
        // Y cambiamos el icono
        document.getElementById("nav-handle-icon").classList.remove("fa-chevron-down");
        document.getElementById("nav-handle-icon").classList.add("fa-chevron-up");
    }
}

// Verifica que los enlaces se estan mostrando cuando se esta en escritorio,
// y que estan ocultos cuando se esta en movil
function nav_check_width() {
    // Si la anchura es mayor de 1100, estamos en escritorio
    if (window.innerWidth > 1100) {
        var l = document.getElementsByClassName("nav-link");
        for (let i of l) {
            i.style.display = "block";
        }
    }
    // Si no, estamos en movil
    if (window.innerWidth <= 1100) {
        // Ocultamos los links
        var l = document.getElementsByClassName("nav-link");
        for (let i of l) {
            i.style.display = "none";
        }
        // Ponemos el icono del handle hacia abajo
        document.getElementById("nav-handle-icon").classList.remove("fa-chevron-up");
        document.getElementById("nav-handle-icon").classList.add("fa-chevron-down");
    }
}
