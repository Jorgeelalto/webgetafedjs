// Cambia las dimensiones del carrusel y sus elementos
function car_check_width() {
    // Ponemos la altura del carusel como 0.5 de la anchura
    // Primero conseguimos la anchura del div
    let w = window.getComputedStyle(document.getElementById("carousel")).width;
    // Ahora quitamos el px...
    w = w.slice(0, -2);
    // ... dividimos entre 2...
    w = 0.5 * parseInt(w, 10);
    // ... y volvemos a poner el px
    w = w.toString() + "px";
    // Finalmente lo cambiamos
    document.getElementById("carousel").style.height = w;

    // Ahora ponemos la anchura de cada elemento del carrusel igual a la del contenedor del carrusel
    for (let i of document.getElementsByClassName("car-item")) {
        i.style.width = window.getComputedStyle(document.getElementById("carousel")).width;
    }
}

// Hace que el carrusel cambie automaticamente de imagen
function move_carousel() {
    // El elemento actual lo ocultamos
    document.getElementsByClassName("car-item")[cur_item].style.opacity = 0.0;
    // Cogemos el siguiente elemento
    cur_item++;
    // Miramos si hemos llegado al final
    if (cur_item >= document.getElementsByClassName("car-item").length) {
        cur_item = 0;   
    }
    // y lo mostramos
    document.getElementsByClassName("car-item")[cur_item].style.opacity = 1.0;
}

// Necesitamos saber en que elemento del carrusel estamos
let cur_item = 0;
// Conseguimos la lista de los elementos
document.getElementsByClassName("car-item");
// Mostramos el primer elemento
document.getElementsByClassName("car-item")[0].style.opacity = 1.0;
// Ejecuta la funcion cada 3000ms
setInterval(move_carousel, 3000);